class Snake 
{
    constructor()
    {
        this._body = [[1,1],[1,2],[1,3]];
        this._direct = 0;
        this.direction = [
            [0,1], //right
            [1,0], //down
            [0,-1], //left
            [-1,0] //up
        ];
        this._isAlive = true;
    }
    get body()
    {
        return this._body;
    }
    get isAlive()
    {
        return this._isAlive;
    }
    death()
    {
        this._isAlive = false;
    }
    async move()
    {
        //Find head of the snake
        let headCell = this.recalculateHead();
        gameMain.check_collision(headCell);
        return await headCell;
    }
    recalculateHead()
    {
        let h = this._body[this._body.length-1];
        let head = [];
        for(let i = 0; i < h.length; i++)
        {
            head[i] = h[i] + this.direction[this._direct][i];
        }
        return head;
    }
    increase(headCell)//Snake lenght ++      
    {
        this._body.push(headCell);
    }
    decrease(headCell) //Snake lenght --
    {
        if(this._body.length > 2)
        {
            this._body.shift();
            this._body.shift();
            this._body.push(headCell);                    
        }
        else
        {
            this._body.shift();
            this._body.push(headCell);
        }
    }
    out_of_border(headCell) // Check out of border
    {
        let HeadTmp = document.getElementById(headCell.join()); 
        if (HeadTmp == null ) 
        {
          if (headCell[0] == -1)
            headCell[0] = gameMain.width - 1;
          if (headCell[0] == gameMain.width)
            headCell[0] = 0;
          if (headCell[1] == -1)
            headCell[1] = gameMain.height - 1;
          if (headCell[1] == gameMain.height)
            headCell[1] = 0;          
        }
        return document.getElementById(headCell.join());
    }
    step(headCell)
    {
        let removeTail = this._body.shift();
        this._body.push(headCell);
    }
    up()
    {
        if (this._direct != 1)            
            this._direct = 3;
    }
    down()
    {
        if (this._direct != 3)
            this._direct = 1;
    }
    left()
    {
        if (this._direct != 0)
            this._direct = 2;
    }
    right()
    {
        if (this._direct != 2)
            this._direct = 0;
    }        
}
class Food 
{
    constructor()
    {
        this.x = 0;
        this.y = 0;
    }
    destroy(){}
}
class GreenFood extends Food
{
    constructor()
    {
        super();
    }
    destroy(headCell)
    {
        gameMain.add_score(1);
        gg.increase(headCell);
        gameMain.post_food(new GreenFood());
    }
}
class RedFood extends Food
{
    constructor()
    {
        super();
    }
    destroy(headCell)
    {
        gameMain.add_score(2);
        gg.decrease(headCell);
        gameMain.post_food(new RedFood());
    }
}
class BlueFood extends Food
{
    constructor()
    {
        super();
    }
    destroy(headCell)
    {
        gg.death();
    }
}
class Graphics
{
    draw_table()
    {
        for ( let x = 0; x < gameMain.width; x++){
            let coordinateX = document.createElement('div');
            //document.body.appendChild(coordinateX);
            $('#border').append(coordinateX);
            coordinateX.className = 'field';
            for (let y = 0; y < gameMain.height; y++){
                let coordinateY = document.createElement('div');
                coordinateX.appendChild(coordinateY);
                coordinateY.className = 'cell';
                coordinateY.id = x+','+y;
            }
        }
    }
    draw_death_screen()
    {
        $('#DefeatScreen').slideToggle('slow');
        setTimeout(() => {
            $('#DefeatScreen').slideToggle();
        }, 2000);
    }
    draw_win_screen()
    {
        $('#WinScreen').slideToggle('slow');
        setTimeout(() => {
            $('#WinScreen').slideToggle();
        }, 2000);
    }
    draw_snake()
    {
        //Pass through all the components of the snake
        for ( let i = 0; i < gg.body.length; i++)
        {
            let currentBodyPart = gg.body[i];
            //Assigned to the cells of the snake style
            document.getElementById(currentBodyPart.join()).className = 'cell snake';
        }
    }
    clear_table()
    {
        for(let x = 0; x < gameMain.width; x++)
        {
            for(let y = 0; y < gameMain.height; y++)
            {
                document.getElementById(x+','+y).className = 'cell';
            }
        }
    }
    draw_food(food_list)
    {
        for(let index in food_list)
        {
            document.getElementById(food_list[index].x+','+food_list[index].y).className = this.changeType(food_list[index]);
        }
    }
    changeType(obj)
    {
        if(obj instanceof RedFood)
            return 'cell redfood';
        if(obj instanceof GreenFood)
            return 'cell food';
        if(obj instanceof BlueFood)
            return 'cell death';
    }
    draw_score(score)
    {
        $('#Score').html('Score: ' + score);
    }
}
class Game
{
    constructor(gg)
    {
        this.graphic = new Graphics();
        this.food_list = [];
        this.snake = gg;
        this.fieldSizeX = 20;
        this.fieldSizeY = 20;
        this._score = 0;
    }
    get width()
    {
        return this.fieldSizeX;
    }
    get height()
    {
        return this.fieldSizeY;
    }
    prepareGamePane()
    {
        this.graphic.draw_table();
        this.graphic.draw_snake();
    }
    reload() //reset game
    {
        window.location.reload();
    }
    Tick() {
        if(this.snake.isAlive)
        {
            this.check_win();
            this.snake.move();
            this.graphic.clear_table();
            this.graphic.draw_snake();
            this.graphic.draw_food(this.food_list);
        }
        else
            this.defeat();        
    }
    async add_score(value){
        this._score = this._score + value;
        await this.graphic.draw_score(this._score);
    }
    check_win()
    {
        let count = 0;
        for (let x = 0; x < gameMain.width; x++)
            for(let y = 0; y < gameMain.height; y++)
                if (document.getElementById(x+','+y).className == 'cell')
                    count++;

        if(count == 0)
            this.win();
    }
    win()
    {
        clearInterval(timer);
        this.graphic.draw_win_screen();
    }
    defeat()
    {
        clearInterval(timer);
        this.graphic.draw_death_screen();
    }
    check_collision(headCell) {
        let tmp = this.snake.out_of_border(headCell);
         //If cells empty draw snake
        if ( tmp != null && tmp.className == 'cell' )
        {
            this.snake.step(headCell);
        }
        else if(tmp.className == 'cell snake'){//If snake eats itself
            this.snake.death();
        }
        else //if cells with something
        {
            for(let index in this.food_list)
            {
                if(document.getElementById(this.food_list[index].x+','+this.food_list[index].y) == tmp)
                {
                    this.food_list[index].destroy(headCell);
                    this.delete_from_table(index);
                }
            }
        }
    }
    delete_from_table(index)
    {
        this.food_list.splice(index,1);
    }
    add_to_food_list(food)
    {
        this.food_list.push(food);
        //console.dir(this.food_list);
    }
    async post_food(obj)
    {
        //Generated x coordinate
        obj.x = Math.round(Math.random() * (this.fieldSizeX-1));
        //Generated y coordinate
        obj.y = Math.round(Math.random() * (this.fieldSizeY-1));
        let food = document.getElementById(obj.x+','+obj.y);
        //If the cell is not occupied
        if (food.className == 'cell'){
            for(let index in this.food_list)
                if(this.food_list[index].x == obj.x && this.food_list[index].y == obj.y)
                    this.post_food(obj);
        } 
         else { //try again
            this.post_food(obj);
        }
        this.add_to_food_list(obj);
        return await food;
    }
}
//Check pressed key
window.addEventListener('keydown', keyHandler, false);
const KEY = {
    'left' : 37,
    'up' : 38,
    'right' : 39,
    'down' : 40
};
function keyHandler (event){ 
        switch (event.keyCode) {
            case KEY.left:
                gg.left();
                break;
            case KEY.right:
                gg.right();
                break;
            case KEY.up:
                gg.up();
                break;
            case KEY.down:
                gg.down();
                break;
            default :
                return;
        }
}
    const gg = new Snake();
    const gameMain = new Game(gg);
    gameMain.prepareGamePane();
    
    
let timer;
let GameOn = false;
$('#startgame').click(async function()
    {
        if(!GameOn)
        {
            let green_food = $('#grCount').val();            
            for (let i=0; i < green_food; i++)
                await gameMain.post_food(new GreenFood());

            let red_food = $('#rdCount').val();
            for(let l=0; l < red_food; l++)
                await gameMain.post_food(new RedFood());

            let blue_food = $('#blCount').val();
            for(let k=0; k < blue_food; k++)
                await gameMain.post_food(new BlueFood());


            timer = setInterval(function(){gameMain.Tick()}, 200);

            GameOn = true;
        }
        
    });
$('#reload').click(function()
{
    gameMain.reload();
});